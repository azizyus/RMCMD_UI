import curses
import curses.panel
from time import sleep

stdscr = curses.initscr()
# Setup screen object
curses.cbreak()  # No need for [Return]
#curses.noecho()  # Stop keys being printed
curses.curs_set(0)  # Invisible cursor
stdscr.keypad(True)
stdscr.clear()
#               format: (lines, cols, y, x)
window_one = curses.newwin(10, 20, 1, 1)
window_two = curses.newwin(5, 20, 5, 40)

# Make windows clearly visible
window_one.addstr(2, 2, "Window One")
window_one.border(0)

window_two.addstr(2, 2, "Window Two")
window_two.border(0)

# Create panels
panel_one = curses.panel.new_panel(window_one)
panel_two = curses.panel.new_panel(window_two)

# Both hidden by default
display_one = False
display_two = False


i=0
screen = 1
while True:


    key = 0
    if screen == 1:
        key =  stdscr.getch()
    elif screen == 2:
        key = window_one.getch()
    elif screen == 3:
        key = window_two.getch()

    if key == 27:
        break
    elif chr(key) == "1":
        screen = 1
    elif chr(key) == "2":
        screen = 2
    elif chr(key) == "3":
        screen = 3

    window_one.refresh()
    panel_one.show()
    #window_one.addstr(2,2,"TEST "+str(i))
    i=i+1

    window_two.refresh()
    panel_two.show()
    window_two.addstr(2, 2, "TEST_2 " + str(i))



